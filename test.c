/**************************************************************************
 *
 *  File:       test.c
 *
 *  Project:    Dab (https://gitlab.com/qrdl/dab)
 *
 *  Descr:      DB-independent data access library
 *
 *  Comments:   Test and sample
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "eel.h"
#include "dab.h"
#ifdef STINGRAY
#include "stingray.h"
#endif

#if defined SQLITE
#define AUTOINCREMENT "AUTOINCREMENT"
#elif defined MYSQL
#define AUTOINCREMENT "AUTO_INCREMENT"
#else
#error "Database not specified"
#endif

FILE *logfd;

int query(int from, int to) {
    static void *cursor = NULL;
    int ret = DAB_OK;

    if (!cursor) {
        if (DAB_OK != DAB_CURSOR_OPEN(&cursor, "SELECT foo, bar FROM fubar "
                    "WHERE foo >= ? AND foo <= ?", from, to))
            RETCLEAN(DAB_FAIL);
    } else if (DAB_OK != DAB_CURSOR_BIND(cursor, from, to))
        RETCLEAN(DAB_FAIL);

    printf("From %d to %d:\n", from, to);

    int foo;
#ifdef STINGRAY
    sr_string bar = sr_new("", 16);
    while (DAB_OK == (ret = DAB_CURSOR_FETCH(cursor, &foo, bar))) {
        printf("%d => %s\n", foo, CSTR(bar));
    }
    STRFREE(bar);
#else
    char *bar;
    while (DAB_OK == (ret = DAB_CURSOR_FETCH(cursor, &foo, &bar))) {
        printf("%d => %s\n", foo, bar);
        free(bar);
    }
#endif

cleanup:
    if (cursor)
        DAB_CURSOR_RESET(cursor);

    return DAB_NO_DATA == ret ? DAB_OK : ret;
}

void usage(char *prog) {
    printf("Usage: %s -d<database name> [-u<user] [-p<password>] "
            "[-h<host>] [-t<port> | -s<socket>]\n", prog);
}

int main(int argc, char *argv[]) {
    char *db = NULL;
    char *user = NULL;
    char *password = NULL;
    char *host = NULL;
    char *socket = NULL;
    unsigned int port = 0;
    int opt;

    logfd = stderr;

    while ((opt = getopt(argc, argv, "d:u:p:h:t:s:")) != -1 ) {
        switch (opt) {
            case 'd' : db = optarg;
                       break;
            case 'u' : user = optarg;
                       break;
            case 'p' : password = optarg;
                       break;
            case 'h' : host = optarg;
                       break;
            case 't' : port = strtoul(optarg, NULL, 10);
                       break;
            case 's' : socket = optarg;
                       break;
            default:   usage(argv[0]);
                       return EXIT_FAILURE;
        }
    }

    if (!db) {
        usage(argv[0]);
        return EXIT_FAILURE;
    }

#define OPT_SIZE(A) (A ? strlen(A) + sizeof(#A) + 2 : 0)
    int len = OPT_SIZE(user) + OPT_SIZE(password) + OPT_SIZE(host) + OPT_SIZE(socket);
    if (!socket && port)
        len += sizeof("port") + 5 + 2;

    char *connect_string = NULL;
    if (len) {
        connect_string = malloc(len+1);
        char *tail = connect_string;
#define ADD_OPT(A) if (A) tail += sprintf(tail, #A "=%s;", A)
        ADD_OPT(user);
        ADD_OPT(password);
        ADD_OPT(host);
        ADD_OPT(socket);
        if (!socket && port)
            tail += sprintf(tail, "port=%d;", port);
        // remove trailing ';'
        tail[-1] = '\0';
    }

    int ret = DAB_OPEN(db, DAB_FLAG_CREATE, connect_string);
    if (DAB_OK != ret)
        return EXIT_FAILURE;

    if (DAB_OK != DAB_EXEC("CREATE TABLE IF NOT EXISTS fubar ("
                "foo INTEGER PRIMARY KEY " AUTOINCREMENT ", "
                "bar VARCHAR(32))"))
        return EXIT_FAILURE;

    char tmp[] = "foo ";
    int i;
    DAB_TXN;
    if (DAB_OK != DAB_BEGIN)
        return EXIT_FAILURE;
    for (i = 0; i < 27; i++) {
        tmp[3] = 'A' + i;
        if (DAB_OK != DAB_EXEC("INSERT INTO fubar (bar) VALUES (?)", tmp)) {
            DAB_ROLLBACK;
            return EXIT_FAILURE;
        }
    }
    if (DAB_OK != DAB_COMMIT)
        return EXIT_FAILURE;

    if (DAB_OK != query(5, 10))
        return EXIT_FAILURE;

    if (DAB_OK != query(15, 20))
        return EXIT_FAILURE;

/*    if (DAB_OK != DAB_EXEC("DROP table fubar"))
        return EXIT_FAILURE; */

    if (DAB_OK != DAB_CLOSE(DAB_FLAG_GRACEFUL))
        return EXIT_FAILURE;

    printf("ok\n");

    return EXIT_SUCCESS;
}

