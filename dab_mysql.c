/**************************************************************************
 *
 *  File:       dab_mysql.c
 *
 *  Project:    Dab (https://gitlab.com/qrdl/dab)
 *
 *  Descr:      DB-independent data access library
 *
 *  Comments:   MySQL / MariaDB access
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#include <mysql.h>
#include <stdarg.h>
#include <stdlib.h>
#include <errno.h>
#include "dab.h"
/* Requires EEL error handling and logging lib - https://gitlab.com/qrdl/eel */
#include "eel.h"
#ifdef STINGRAY
#include "stingray.h"
#endif

#define STMT(A) ((MYSQL_STMT *)(A))
#define P_STMT(A) ((MYSQL_STMT **)(A))
#define UNUSED(A) (void)(A)

#define DBERR(A) LOCAL_LOG('E', file, line, "%s - %s (%d)", (A), \
        mysql_error(db), mysql_errno(db))
#define STMTERR(A) LOCAL_LOG('E', file, line, "%s - %s (%d)", (A), \
        mysql_stmt_error(stmt), mysql_stmt_errno(stmt))

static int sql_common(const char *file, int line, MYSQL_STMT **stmt,
        const char *stmt_text, va_list ap, MYSQL_BIND **bind_out);
static int cursor_bind(const char *file, int line, MYSQL_STMT *stmt,
        va_list ap, MYSQL_BIND **bind_out);
static void free_bind(MYSQL_STMT *stmt, MYSQL_BIND *bind);

static MYSQL *db = NULL;        // DB handle

static int in_txn = 0;

#ifndef NOTHREADS
#include "dab_threads.h"
static pthread_mutex_t txn_mutex;
static pthread_rwlock_t db_lock = PTHREAD_RWLOCK_INITIALIZER;
#else
#define READ_LOCK(F,L)
#define WRITE_LOCK(F,L)
#define UNLOCK(F,L)
#define TXN_LOCK(F,L)
#define TXN_UNLOCK(F,L)
#endif

/**************************************************************************
 *
 *  Function:   dab_open
 *
 *  Params:     db_name - database file name
 *              flags - see DAB_FLAG_XXX
 *              user - user's name
 *              passwd - user's password
 *              host - host's name / address
 *              port - host's port
 *              sock - local socket or pipe name (used if port is 0)
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL / DAB_MALFUNCTION
 *
 *  Descr:      Connect to db using provided credentials
 *
 **************************************************************************/
int dab_open(const char *file, int line, const char *db_name, ULONG flags,
        const char *connect_string) {
    char *user = NULL;
    char *password = NULL;
    char *host = NULL;
    char *socket = NULL;
    char *port = NULL;
    int ret = DAB_OK;
    UNUSED(flags);

    if (!db_name || !*db_name || !connect_string || !*connect_string)
        return DAB_INVALID;

    WRITE_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (db) {
        LOCAL_LOG('E', file, line, "DB is already opened");
        RETCLEAN(DAB_INVALID);
    }

    char *params = strdup(connect_string);
    char *tmp, *token, *equal;
    for (token = strtok_r(params, ";", &tmp); token;
            token = strtok_r(NULL, ";", &tmp)) {
        equal = strchr(token, '=');
        if (!equal || !equal[1])
            continue;   // option without "=<value>" or "<value>" part - skip
        if (!user && !strncmp("user", token, equal - token))
            user = equal + 1;
        else if (!password && !strncmp("password", token, equal - token))
            password = equal + 1;
        else if (!host && !strncmp("host", token, equal - token))
            host = equal + 1;
        else if (!socket && !strncmp("socket", token, equal - token))
            socket = equal + 1;
        else if (!port && !strncmp("port", token, equal - token))
            port = equal + 1;
    }

    if (!port && !socket) {
        LOCAL_LOG('E', file, line, "Either port or socket must be specified");
        RETCLEAN(DAB_INVALID);
    }

    db = mysql_init(NULL);
    if (!db)
        RETCLEAN(DAB_FAIL);

    /* connect to server, but not specifying db yet */
    if (!mysql_real_connect(db, host, user, password, db_name,
                port ? atoi(port) : 0, socket, 0)) {
        DBERR("Error opening DB");
        RETCLEAN(DAB_FAIL);
    }

#ifndef NOTHREADS
    pthread_mutexattr_t attr;
    int thread_err;
    thread_err = pthread_mutexattr_init(&attr);
    if (thread_err) {
        LOCAL_LOG('E', file, line, "Mutex attr init failed - %s",
                strerror(thread_err));
        RETCLEAN(DAB_MALFUNCTION);
    }
    thread_err = pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    if (thread_err) {
        LOCAL_LOG('E', file, line, "Mutex attr recursive failed - %s",
                strerror(thread_err));
        RETCLEAN(DAB_MALFUNCTION);
    }
    thread_err = pthread_mutex_init(&txn_mutex, &attr);
    if (thread_err) {
        LOCAL_LOG('E', file, line, "Txn mutex init failed - %s",
                strerror(thread_err));
        RETCLEAN(DAB_MALFUNCTION);
    }
#endif

cleanup:
    if (DAB_OK != ret && db) {
        mysql_close(db);
        db = NULL;
    }
    UNLOCK(file, line);
    free(params);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_close
 *
 *  Params:     flags - see DAB_FLAG_DEFER
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_MALFUNCTION
 *
 *  Descr:      Close DB connection
 *
 *  Note:       In case of graceful close connection still be active
 *              until all prepared statements are freed, but will not be
 *              reachable for new operations, DB needs to be re-opened
 *              to run new statements.
 *
 **************************************************************************/
int dab_close(const char *file, int line, int flag) {
    int ret = DAB_OK;
    UNUSED(flag);
#ifdef NOTHREADS
    UNUSED(file);
    UNUSED(line);
#endif

    WRITE_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_OK);      // already closed - nothing to do

    mysql_close(db);
    db = NULL;

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_exec
 *
 *  Params:     stmt_text - SQL statement text
 *              ... - pair of params, each pair consists of type ID and
 *                    parameter
 *                    type 0 means "no more params"
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      My own MariaDB one-function exec that does everything -
 *              prepares the statement, binds all params, executes the
 *              statement and frees it
 *              Because statement is prepared, there is no need to protect
 *              from SQL injection
 *
 **************************************************************************/
int dab_exec(const char *file, int line, const char *stmt_text, ...) {
    int ret = DAB_OK;
    MYSQL_STMT *stmt = NULL;
    va_list ap;

    if (!stmt_text || !*stmt_text)
        return DAB_INVALID;

    va_start(ap, stmt_text);

    MYSQL_BIND *bind = NULL;
    ret = sql_common(file, line, &stmt, stmt_text, ap, &bind);
    if (DAB_OK != ret)
        RETCLEAN(ret);

    if (mysql_stmt_execute(stmt)) {
        DBERR("Error executing statement");
        RETCLEAN(DAB_FAIL);
    } else
        ret = DAB_OK;

cleanup:
    va_end(ap);
    free_bind(stmt, bind);

    if (stmt)
        mysql_stmt_close(stmt);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_open
 *
 *  Params:     stmt (OUT) - where to store prepared statement
 *              stmt_text - SQL statement text
 *              ... - pair of params, each pair consists of type ID and
 *                    parameter
 *                    type 0 means "no more params"
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      Create and bind the cursor
 *
 **************************************************************************/
int dab_cursor_open(const char *file, int line, void **cursor,
        const char *stmt_text, ...) {
    int ret = DAB_OK;
    va_list ap;

    if (!cursor || !stmt_text || !*stmt_text)
        return DAB_INVALID;

    va_start(ap, stmt_text);

    MYSQL_BIND *bind = NULL;
    ret = sql_common(file, line, P_STMT(cursor), stmt_text, ap, &bind);
    if (DAB_OK != ret)
        RETCLEAN(ret);

    va_end(ap);

    MYSQL_STMT *stmt = *P_STMT(cursor);

    if (mysql_stmt_execute(stmt)) {
        STMTERR("Error executing statement");
        mysql_stmt_close(stmt);
        RETCLEAN(DAB_FAIL);
    }

cleanup:
    free_bind(stmt, bind);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_last_id
 *
 *  Params:     
 *
 *  Return:     rowid of last inserted record / 0 on error
 *
 *  Descr:      Return ID of last inserted record, if it has AUTOINCREMENT
 *              column
 *
 **************************************************************************/
ULONG dab_last_id(const char *file, int line) {
    int ret = DAB_OK;

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return 0;

    if (!db)
        return 0;

    ULONG row = (ULONG)mysql_insert_id(db);
    if (!row)
        DBERR("Nothing was inserted into DB");

    UNLOCK(file, line);

    if (DAB_OK != ret)
        return 0;
    else
        return row;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_bind
 *
 *  Params:     stmt - prepared SQL statement
 *              ... - pair of params, each pair consists of type ID and
 *                    parameter
 *                    type 0 means "no more params"
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      Re-bind previously created cursor
 *
 **************************************************************************/
int dab_cursor_bind(const char *file, int line, void *cursor, ...) {
    va_list ap;
    int ret = DAB_OK;

    if (!cursor)
        return DAB_INVALID;

    va_start(ap, cursor);

    MYSQL_BIND *bind = NULL;
    MYSQL_STMT *stmt = STMT(cursor);
    ret = cursor_bind(file, line, stmt, ap, &bind);
    if (DAB_OK != ret)
        RETCLEAN(ret);

    va_end(ap);

    if (mysql_stmt_execute(stmt)) {
        STMTERR("Error executing statement");
        mysql_stmt_close(stmt);
        RETCLEAN(DAB_FAIL);
    }

cleanup:
    free_bind(stmt, bind);
    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_fetch
 *
 *  Params:     stmt - prepared and bound SQL statement
 *              ... - pair of params, each pair consists of type ID and
 *                    parameter
 *                    type 0 means "no more params"
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL / DAB_NO_DATA
 *
 *  Descr:      Fetch a row from cursor
 *
 **************************************************************************/
int dab_cursor_fetch(const char *file, int line, void *stmt, ...) {
    int ret = DAB_OK;
    va_list ap;
    int type, index = 0;
    int has_string = 0;

    if (!stmt)
        return DAB_INVALID;

    int var_count = mysql_stmt_field_count(STMT(stmt));
    if (!var_count)
        return DAB_OK;      // nothing to fetch


    MYSQL_BIND *bind = calloc(var_count, sizeof(*bind));

    va_start(ap, stmt);

    for (type = va_arg(ap, int); type; type = va_arg(ap, int)) {
        if (index >= var_count)
            break;      // too many params - handle after the loop

        switch (type) {
            case DB_DATATYPE_CHR:
            case DB_DATATYPE_UCHR:
                bind[index].buffer = va_arg(ap, char *);
                bind[index].buffer_type = MYSQL_TYPE_TINY;
                break;
            case DB_DATATYPE_SHRT:
            case DB_DATATYPE_USHRT:
                bind[index].buffer = va_arg(ap, short *);
                bind[index].buffer_type = MYSQL_TYPE_SHORT;
                break;
            case DB_DATATYPE_INT:
            case DB_DATATYPE_UINT:
                bind[index].buffer = va_arg(ap, int *);
                bind[index].buffer_length = sizeof(int);
                bind[index].buffer_type = MYSQL_TYPE_LONG;
                break;
            case DB_DATATYPE_LNG:
            case DB_DATATYPE_ULNG:
                bind[index].buffer = va_arg(ap, long *);
                bind[index].buffer_type = MYSQL_TYPE_LONG;
                break;
            case DB_DATATYPE_LLNG:
            case DB_DATATYPE_ULLNG:
                bind[index].buffer = va_arg(ap, long long *);
                bind[index].buffer_type = MYSQL_TYPE_LONGLONG;
                break;
            case DB_DATATYPE_FLT:
                bind[index].buffer = va_arg(ap, float *);
                bind[index].buffer_type = MYSQL_TYPE_FLOAT;
                break;
            case DB_DATATYPE_DBL:
                bind[index].buffer = va_arg(ap, double *);
                bind[index].buffer_type = MYSQL_TYPE_DOUBLE;
                break;
            case DB_DATATYPE_STR:
            case DB_DATATYPE_USTR:
                bind[index].buffer = va_arg(ap, char **);
                bind[index].buffer_type = MYSQL_TYPE_STRING;
                bind[index].buffer_length = 0;
                bind[index].is_unsigned = 0;    // hack - needed to distinguish between
                                                // C standard and Stingray strings
                unsigned long *tmp = malloc(sizeof(*tmp));
                *tmp = 0;
                bind[index].length = tmp;
                has_string++;
                break;
#ifdef STINGRAY
            case DB_DATATYPE_SR: {
                    sr_string sr = va_arg(ap, sr_string);
                    bind[index].buffer = (void *)sr;
                    bind[index].buffer_type = MYSQL_TYPE_STRING;
                    bind[index].buffer_length = 0;
                    bind[index].is_unsigned = 1;    // hack - see comment above
                    unsigned long *tmp = malloc(sizeof(*tmp));
                    *tmp = 0;
                    bind[index].length = tmp;
                    has_string++;
                    break;
                }
#endif
            default:
                LOCAL_LOG('E', file, line, "Invalid column type for param");
                RETCLEAN(DAB_INVALID);
                break;
        }
        index++;
    }

    /* check if number of '?' placeholders actually match the number of
     * params */
    if (index != var_count) {
        LOCAL_LOG('E', file, line, "Number of params doesn't match number of "
                "placeholders");
        RETCLEAN(DAB_INVALID);
    }

    if (mysql_stmt_bind_result(STMT(stmt), bind)) {
        STMTERR("Error binding result");
        RETCLEAN(DAB_FAIL);
    }

    ret = mysql_stmt_fetch(STMT(stmt));
    if (ret && MYSQL_DATA_TRUNCATED != ret) {
        if (MYSQL_NO_DATA)
            RETCLEAN(DAB_NO_DATA);
        STMTERR("Error fetching data");
        RETCLEAN(DAB_FAIL);
    }
    ret = DAB_OK;

    /* for strings allocate big enough buffer and retrieve columns */
    for (index = 0; has_string && index < var_count; ++index) {
        if (MYSQL_TYPE_STRING == bind[index].buffer_type) {
#ifdef STINGRAY
            if (bind[index].is_unsigned) {      // Stingray string
                sr_string sr = (sr_string)bind[index].buffer;
                sr_ensure_size(sr, *bind[index].length + 1, 0);
                bind[index].buffer_length = *bind[index].length;
                bind[index].buffer = CSTR(sr);
                if (mysql_stmt_fetch_column(STMT(stmt), &bind[index], index, 0)) {
                    STMTERR("Error getting string column");
                    RETCLEAN(DAB_FAIL);
                }
                CSTR(sr)[*bind[index].length] = '\0';
                SETLEN(sr, *bind[index].length);
                free(bind[index].length);
                has_string--;
            } else {                            // C standard string
#endif
                /* by now bind[index].buffer has an address of host variable to
                 * store the string to */
                char **host_var = (char **)bind[index].buffer;
                char *tmp = malloc(*bind[index].length+1);
                *host_var = tmp;
                bind[index].buffer = tmp;
                bind[index].buffer_length = *bind[index].length;
                if (mysql_stmt_fetch_column(STMT(stmt), &bind[index], index, 0)) {
                    STMTERR("Error getting string column");
                    RETCLEAN(DAB_FAIL);
                }
                tmp[*bind[index].length] = '\0';
                free(bind[index].length);
                has_string--;
#ifdef STINGRAY
            }
#endif
        }
    }

cleanup:
    va_end(ap);

    /* has_string isn't 0 only if something went wrong while getting
     * string column data */
    for (index = 0; has_string && index < var_count; ++index) {
        if (MYSQL_TYPE_STRING == bind[index].buffer_type) {
            free(bind[index].length);
            has_string--;
        }
    }

    free(bind);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_reset
 *
 *  Params:     stmt - prepared and bound SQL statement
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      Reset the cursor, but do not destroy it. To be used again,
 *              cursor must be bound first
 *
 **************************************************************************/
int dab_cursor_reset(const char *file, int line, void *stmt) {
    if (!stmt)
        return DAB_INVALID;

    if (mysql_stmt_reset(STMT(stmt))) {
        STMTERR("Error resetting stmt");
        return DAB_FAIL;
    }

    return DAB_OK;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_free
 *
 *  Params:     stmt - prepared SQL statement
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      Destory the cursor
 *
 **************************************************************************/
int dab_cursor_free(const char *file, int line, void *stmt) {
    if (!stmt)
        return DAB_INVALID;

    if (mysql_stmt_close(STMT(stmt))) {
        DBERR("Error freeing stmt");
        return DAB_FAIL;
    }

    return DAB_OK;
}


/**************************************************************************
 *
 *  Function:   sql_common
 *
 *  Params:     stmt (OUT) - where to store prepared statement
 *              stmt - SQL statement text
 *              ap - va_list of params
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID
 *
 *  Descr:      Common code for dab_exec() and dab_cursor_open()
 *
 **************************************************************************/
int sql_common(const char *file, int line, MYSQL_STMT **stmt_out,
        const char *stmt_text, va_list ap, MYSQL_BIND **bind) {
    int ret = DAB_OK;

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_INVALID);

    *stmt_out = NULL;
    MYSQL_STMT *stmt = mysql_stmt_init(db);
    if (!stmt) {
        DBERR("Cannot init DB statement");
        return DAB_FAIL;
    }
    if (mysql_stmt_prepare(stmt, stmt_text, strlen(stmt_text))) {
        STMTERR("Error preparing statement");
        mysql_stmt_close(stmt);
        return DAB_FAIL;
    }
    *stmt_out = stmt;

    ret = cursor_bind(file, line, stmt, ap, bind);

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   cursor_bind
 *
 *  Params:     stmt - prepared SQL statement
 *              ap - va_list of params
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID
 *
 *  Descr:      Common code for dab_cursor_bind() and sql_common()
 *
 **************************************************************************/
int cursor_bind(const char *file, int line, MYSQL_STMT *stmt, va_list ap,
        MYSQL_BIND **bind_out) {
    int type, index = 0;
    int ret = DAB_OK;;
    int var_count = mysql_stmt_param_count(stmt);

    if (!var_count)
        return DAB_OK;  // nothing to bind 

    MYSQL_BIND *bind = calloc(var_count, sizeof(*bind));

    for (type = va_arg(ap, int); type; type = va_arg(ap, int)) {
        if (index >= var_count)
            break;      // too many params - handle after the loop

        switch (type) {
		/* it is not very effective to allocate buffers, but MySQL
		 * expects pointer to array, and Dab API doesn't take
		 * addresses for variables to be bound */
#define TMP_BUF(A,B) 	bind[index].buffer = malloc(sizeof(A)); \
			*(A *)bind[index].buffer = (A)va_arg(ap, B);
            case DB_DATATYPE_CHR:
            case DB_DATATYPE_UCHR:
		TMP_BUF(char, int);
                bind[index].buffer_type = MYSQL_TYPE_TINY;
                bind[index].buffer_length = sizeof(char);   // 1 by standard
                break;
            case DB_DATATYPE_SHRT:
            case DB_DATATYPE_USHRT:
		TMP_BUF(short, int);
                bind[index].buffer_type = MYSQL_TYPE_SHORT;
                bind[index].buffer_length = sizeof(short);
                break;
            case DB_DATATYPE_INT:
            case DB_DATATYPE_UINT:
		TMP_BUF(int, int);
                bind[index].buffer_type = MYSQL_TYPE_LONG;
                bind[index].buffer_length = sizeof(int);
                break;
            case DB_DATATYPE_LNG:
            case DB_DATATYPE_ULNG:
		TMP_BUF(long, long);
                bind[index].buffer_type = MYSQL_TYPE_LONG;
                bind[index].buffer_length = sizeof(long);
                break;
            case DB_DATATYPE_LLNG:
            case DB_DATATYPE_ULLNG:
                TMP_BUF(long long, long long);
                bind[index].buffer_type = MYSQL_TYPE_LONGLONG;
                bind[index].buffer_length = sizeof(long long);
                break;
            case DB_DATATYPE_FLT:
		TMP_BUF(float, double);
                bind[index].buffer_type = MYSQL_TYPE_FLOAT;
                bind[index].buffer_length = sizeof(double);
                break;
            case DB_DATATYPE_DBL:
		TMP_BUF(double, double);
                bind[index].buffer_type = MYSQL_TYPE_DOUBLE;
                bind[index].buffer_length = sizeof(double);
                break;
            case DB_DATATYPE_STR:
            case DB_DATATYPE_USTR:
                bind[index].buffer = va_arg(ap, char *);
                bind[index].buffer_type = MYSQL_TYPE_STRING;
                bind[index].buffer_length = strlen(bind[index].buffer);
                break;
#ifdef STINGRAY
            case DB_DATATYPE_SR: {
                    sr_string tmp = va_arg(ap, sr_string);
                    bind[index].buffer = CSTR(tmp);
                    bind[index].buffer_type = MYSQL_TYPE_STRING;
                    bind[index].buffer_length = STRLEN(tmp);
                    break;
            }
#endif
            default:
                LOCAL_LOG('E', file, line, "Invalid column type for param");
                RETCLEAN(DAB_INVALID);
                break;
        }
        index++;
    }

    /* check if number of '?' placeholders actually match the number of
     * params */
    if (index != var_count) {
        LOCAL_LOG('E', file, line, "Number of params doesn't match number of "
                "placeholders");
        RETCLEAN(DAB_INVALID);
    }

    if (mysql_stmt_bind_param(stmt, bind)) {
        STMTERR("Error binding params");
        RETCLEAN(DAB_FAIL);
    }

cleanup:
    if (bind_out)
        *bind_out = bind;       // caller myst free the bindings after exec

    return ret;
}


/**************************************************************************
 *
 *  Function:   free_bind
 *
 *  Params:     stmt - prepared SQL statement
 *              bind - array of bound structures
 *
 *  Return:     N/A
 *
 *  Descr:      Free statement param bindings (cannot be used to free
 *              result bindings!)
 *
 **************************************************************************/
void free_bind(MYSQL_STMT *stmt, MYSQL_BIND *bind) {
    int i;
    if (!stmt || !bind)
        return;

    int var_count = mysql_stmt_param_count(stmt);
    for (i = 0; i < var_count; ++i) {
        if (bind[i].buffer && bind[i].buffer_type != MYSQL_TYPE_STRING)
            free(bind[i].buffer);
    }
    free(bind);
}


/**************************************************************************
 *
 *  Function:   dab_begin
 *
 *  Params:     txn - pointer to local transaction indicator
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID
 *
 *  Descr:      Begin local transaction
 *
 **************************************************************************/
int dab_begin(const char *file, int line, int *txn) {
    int ret = DAB_OK;

    if (*txn) {
        LOCAL_LOG('E', file, line, "Local transaction is already in progress");
        return DAB_INVALID;
    }

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;
    
    if (!db)
        RETCLEAN(DAB_INVALID);

    if (!in_txn) {
        TXN_LOCK(file, line);
        if (DAB_OK != ret)
            RETCLEAN(ret);
        if (mysql_query(db, "START TRANSACTION")) {
            DBERR("Cannot start transaction");
            TXN_UNLOCK(file, line);
            if (DAB_OK != ret)
                RETCLEAN(ret);
            else
                RETCLEAN(DAB_FAIL);
        }
        *txn = 1;   // in local transaction
        in_txn = 1; // global indicator
    }

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_commit
 *
 *  Params:     txn - pointer to local transaction indicator
 *
 *  Return:     DAB_OK / DAB_FAIL
 *
 *  Descr:      Commit local transaction
 *
 **************************************************************************/
int dab_commit(const char *file, int line, int *txn) {
    int ret = DAB_OK;

    if (!*txn)
        return DAB_OK;  // not in transaction

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_INVALID);

    if (mysql_query(db, "COMMIT")) {
        DBERR("Cannot commit transaction");
        RETCLEAN(DAB_FAIL);
    }
    *txn = 0;
    in_txn = 0;

    TXN_UNLOCK(file, line);
    if (DAB_OK != ret)
        RETCLEAN(DAB_MALFUNCTION);

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_rollback
 *
 *  Params:     txn - pointer to local transaction indicator
 *
 *  Return:     DAB_OK / DAB_FAIL
 *
 *  Descr:      Rollback local transaction
 *
 **************************************************************************/
int dab_rollback(const char *file, int line, int *txn) {
    int ret = DAB_OK;

    if (!*txn)
        return DAB_OK;  // not in transaction

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_INVALID);

    if (mysql_query(db, "ROLLBACK")) {
        DBERR("Cannot rollback transaction");
        return DAB_FAIL;
    }
    *txn = 0;
    in_txn = 0;

    TXN_UNLOCK(file, line);
    if (DAB_OK != ret)
        RETCLEAN(DAB_MALFUNCTION);

cleanup:
    UNLOCK(file, line);

    return ret;
}

