/**************************************************************************
 *
 *  File:       dab.h
 *
 *  Project:    Dab (https://gitlab.com/qrdl/dab)
 *
 *  Descr:      DB-independent data access library
 *
 *  Comments:   Common declarations
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#ifndef DAB_H
#define DAB_H

#include "dab_internal.h"

#define DAB_OK              1
#define DAB_FAIL            2
#define DAB_NO_DATA         3
#define DAB_INVALID         4
#define DAB_MALFUNCTION     5

#define DAB_FLAG_NONE       0
#define DAB_FLAG_READONLY   1
#define DAB_FLAG_CREATE     2
#define DAB_FLAG_GRACEFUL   4

/*
 * Database manipulation
 */
#define DAB_OPEN(D, F, CS) dab_open(__FILE__, __LINE__, (D), (F), CS)
#define DAB_CLOSE(F) dab_close(__FILE__, __LINE__, F)

/*
 * Execution of SQL statements
 */

/* One-step excution */
#define DAB_EXEC(A, ...) dab_exec(__FILE__, __LINE__, (A), VAR(NUMARGS(__VA_ARGS__), ##__VA_ARGS__))

/* Queries using cursor */
#define DAB_CURSOR_OPEN(C, A, ...) dab_cursor_open(__FILE__, __LINE__, (C), (A), VAR(NUMARGS(__VA_ARGS__), ##__VA_ARGS__))
#define DAB_CURSOR_PREPARE(C, A, ...) dab_cursor_prepare(__FILE__, __LINE__, (C), (A))
#define DAB_CURSOR_BIND(C, ...) dab_cursor_bind(__FILE__, __LINE__, (C), VAR(NUMARGS(__VA_ARGS__), ##__VA_ARGS__))
#define DAB_CURSOR_FETCH(C, ...) dab_cursor_fetch(__FILE__, __LINE__, (C), PVAR(NUMARGS(__VA_ARGS__), ##__VA_ARGS__))
#define DAB_CURSOR_RESET(C) dab_cursor_reset(__FILE__, __LINE__, (C))
#define DAB_CURSOR_FREE(C) do { dab_cursor_free(__FILE__, __LINE__, (C)); C = NULL;} while (0)

/* Last inserted autoincremented ID */
#define DAB_LAST_ID dab_last_id(__FILE__, __LINE__)

/* Number of affected rows */
#define DAB_AFFECTED_ROWS dab_affected_rows()

/*
 * Transactions
 */
#define DAB_TXN   int local_txn = 0;
#define DAB_BEGIN dab_begin(__FILE__, __LINE__, &local_txn)
#define DAB_COMMIT dab_commit(__FILE__, __LINE__, &local_txn)
#define DAB_ROLLBACK dab_rollback(__FILE__, __LINE__, &local_txn)

#endif

