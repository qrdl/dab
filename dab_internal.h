/**************************************************************************
 *
 *  File:       dab_internal.h
 *
 *  Project:    Dab (https://gitlab.com/qrdl/dab)
 *
 *  Descr:      DB-independent data access library
 *
 *  Comments:   Internal stuff, not be used by application code
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#ifndef DAB_INTERNAL_H
#define DAB_INTERNAL_H

#include <stdint.h>
#define ULONG uint64_t

/* Requires EEL error handling and logging lib - https://gitlab.com/qrdl/eel */
#include "eel.h"
#ifdef STINGRAY
#include "stingray.h"
#endif

/*
 * Macros for processing variadic calls, with params of different types *
 */
#define DB_DATATYPE_INT     1
#define DB_DATATYPE_UINT    2
#define DB_DATATYPE_SHRT    3
#define DB_DATATYPE_USHRT   4
#define DB_DATATYPE_LNG     5
#define DB_DATATYPE_ULNG    6
#define DB_DATATYPE_LLNG    7
#define DB_DATATYPE_ULLNG   8
#define DB_DATATYPE_CHR     9
#define DB_DATATYPE_UCHR    10
#define DB_DATATYPE_STR     11
#define DB_DATATYPE_USTR    12
#define DB_DATATYPE_FLT     13
#define DB_DATATYPE_DBL     14
#ifdef STINGRAY_H
#define DB_DATATYPE_SR      15
#endif

/* ##__VA_ARGS__ is GCC extension, also supported by Clang */
#ifndef NUMARGS
#define NUMARGS(...) NUMARGS__(0, ##__VA_ARGS__, 15, 14, 13, 12, 11, 10, 9, 8, \
       7, 6, 5, 4, 3, 2, 1, 0)
#define NUMARGS__(Z, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, \
        _14, _15, N, ...) N
#endif

/* generic param processing for variables to be bound to statement */
#ifdef STINGRAY_H
#define V(A) _Generic((A), \
        int:                    DB_DATATYPE_INT, \
        unsigned int:           DB_DATATYPE_UINT, \
        short:                  DB_DATATYPE_SHRT, \
        unsigned short:         DB_DATATYPE_USHRT, \
        long:                   DB_DATATYPE_LNG, \
        unsigned long:          DB_DATATYPE_ULNG, \
        long long:              DB_DATATYPE_LLNG, \
        unsigned long long:     DB_DATATYPE_ULLNG, \
        char:                   DB_DATATYPE_CHR, \
        unsigned char:          DB_DATATYPE_UCHR, \
        char *:                 DB_DATATYPE_STR, \
        unsigned char *:        DB_DATATYPE_USTR, \
        const char *:           DB_DATATYPE_STR, \
        const unsigned char *:  DB_DATATYPE_USTR, \
        float:                  DB_DATATYPE_FLT, \
        double:                 DB_DATATYPE_DBL, \
        sr_string:              DB_DATATYPE_SR \
), A
#else
#define V(A) _Generic((A), \
        int:                    DB_DATATYPE_INT, \
        unsigned int:           DB_DATATYPE_UINT, \
        short:                  DB_DATATYPE_SHRT, \
        unsigned short:         DB_DATATYPE_USHRT, \
        long:                   DB_DATATYPE_LNG, \
        unsigned long:          DB_DATATYPE_ULNG, \
        long long:              DB_DATATYPE_LLNG, \
        unsigned long long:     DB_DATATYPE_ULLNG, \
        char:                   DB_DATATYPE_CHR, \
        unsigned char:          DB_DATATYPE_UCHR, \
        char *:                 DB_DATATYPE_STR, \
        unsigned char *:        DB_DATATYPE_USTR, \
        const char *:           DB_DATATYPE_STR, \
        const unsigned char *:  DB_DATATYPE_USTR, \
        float:                  DB_DATATYPE_FLT, \
        double:                 DB_DATATYPE_DBL \
), A
#endif

#define  V0(A)                                               0
#define  V1(_1)                                              V(_1),0
#define  V2(_1,_2)                                           V(_1),V(_2),0
#define  V3(_1,_2,_3)                                        V(_1),V(_2),V(_3),0
#define  V4(_1,_2,_3,_4)                                     V(_1),V(_2),V(_3),\
    V(_4),0
#define  V5(_1,_2,_3,_4,_5)                                  V(_1),V(_2),V(_3),\
    V(_4),V(_5),0
#define  V6(_1,_2,_3,_4,_5,_6)                               V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),0
#define  V7(_1,_2,_3,_4,_5,_6,_7)                            V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),0
#define  V8(_1,_2,_3,_4,_5,_6,_7,_8)                         V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),V(_8),0
#define  V9(_1,_2,_3,_4,_5,_6,_7,_8,_9)                      V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),V(_8),V(_9),0
#define V10(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A)                   V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),V(_8),V(_9),V(_A),0
#define V11(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B)                V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),V(_8),V(_9),V(_A),V(_B),0
#define V12(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C)             V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),V(_8),V(_9),V(_A),V(_B),V(_C),0
#define V13(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D)          V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),V(_8),V(_9),V(_A),V(_B),V(_C),V(_D),0
#define V14(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D,_E)       V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),V(_8),V(_9),V(_A),V(_B),V(_C),V(_D),V(_E),0
#define V15(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D,_E,_F)    V(_1),V(_2),V(_3),\
    V(_4),V(_5),V(_6),V(_7),V(_8),V(_9),V(_A),V(_B),V(_C),V(_D),V(_E),V(_F),0

#define VAR_(A) V ## A
#define VAR(A, ...) VAR_(A)(__VA_ARGS__)

/* generic param processing for variables to be populated from statement */
#ifdef STINGRAY_H
#define P(A) _Generic((A), \
        int *:              DB_DATATYPE_INT, \
        unsigned int *:     DB_DATATYPE_UINT, \
        short *:            DB_DATATYPE_SHRT, \
        unsigned short *:   DB_DATATYPE_USHRT, \
        long *:             DB_DATATYPE_LNG, \
        unsigned long *:    DB_DATATYPE_ULNG, \
        long long *:        DB_DATATYPE_LLNG, \
        unsigned long long*:DB_DATATYPE_ULLNG, \
        char *:             DB_DATATYPE_CHR, \
        unsigned char *:    DB_DATATYPE_UCHR, \
        char **:            DB_DATATYPE_STR, \
        unsigned char **:   DB_DATATYPE_USTR, \
        float *:            DB_DATATYPE_FLT, \
        double *:           DB_DATATYPE_DBL, \
        sr_string:          DB_DATATYPE_SR \
), A
#else
#define P(A) _Generic((A), \
        int *:              DB_DATATYPE_INT, \
        unsigned int *:     DB_DATATYPE_UINT, \
        short *:            DB_DATATYPE_SHRT, \
        unsigned short *:   DB_DATATYPE_USHRT, \
        long *:             DB_DATATYPE_LNG, \
        unsigned long *:    DB_DATATYPE_ULNG, \
        long long *:        DB_DATATYPE_LLNG, \
        unsigned long long*:DB_DATATYPE_ULLNG, \
        char *:             DB_DATATYPE_CHR, \
        unsigned char *:    DB_DATATYPE_UCHR, \
        char **:            DB_DATATYPE_STR, \
		const char **:      DB_DATATYPE_STR, \
        unsigned char **:   DB_DATATYPE_USTR, \
		const unsigned char **:   DB_DATATYPE_USTR, \
        float *:            DB_DATATYPE_FLT, \
        double *:           DB_DATATYPE_DBL \
), A
#endif

#define  P0(A)                                               0
#define  P1(_1)                                              P(_1),0
#define  P2(_1,_2)                                           P(_1),P(_2),0
#define  P3(_1,_2,_3)                                        P(_1),P(_2),P(_3),0
#define  P4(_1,_2,_3,_4)                                     P(_1),P(_2),P(_3),\
    P(_4),0
#define  P5(_1,_2,_3,_4,_5)                                  P(_1),P(_2),P(_3),\
    P(_4),P(_5),0
#define  P6(_1,_2,_3,_4,_5,_6)                               P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),0
#define  P7(_1,_2,_3,_4,_5,_6,_7)                            P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),0
#define  P8(_1,_2,_3,_4,_5,_6,_7,_8)                         P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),P(_8),0
#define  P9(_1,_2,_3,_4,_5,_6,_7,_8,_9)                      P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),P(_8),P(_9),0
#define P10(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A)                   P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),P(_8),P(_9),P(_A),0
#define P11(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B)                P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),P(_8),P(_9),P(_A),P(_B),0
#define P12(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C)             P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),P(_8),P(_9),P(_A),P(_B),P(_C),0
#define P13(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D)          P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),P(_8),P(_9),P(_A),P(_B),P(_C),P(_D),0
#define P14(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D,_E)       P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),P(_8),P(_9),P(_A),P(_B),P(_C),P(_D),P(_E),0
#define P15(_1,_2,_3,_4,_5,_6,_7,_8,_9,_A,_B,_C,_D,_E,_F)    P(_1),P(_2),P(_3),\
    P(_4),P(_5),P(_6),P(_7),P(_8),P(_9),P(_A),P(_B),P(_C),P(_D),P(_E),P(_F),0

#define PVAR_(A) P ## A
#define PVAR(A, ...) PVAR_(A)(__VA_ARGS__)

int dab_open(const char *file, int line, const char *db_name, ULONG flags, const char *connect_string);
int dab_close(const char *file, int line, int flag);
int dab_exec(const char *file, int line, const char *stmt_text, ...);
int dab_cursor_open(const char *file, int line, void **cursor, const char *stmt_text, ...);
int dab_cursor_prepare(const char *file, int line, void **cursor, const char *stmt_text);
int dab_cursor_bind(const char *file, int line, void *cursor, ...);
int dab_cursor_fetch(const char *file, int line, void *cursor, ...);
int dab_cursor_reset(const char *file, int line, void *cursor);
int dab_cursor_free(const char *file, int line, void *cursor);
ULONG dab_last_id(const char *file, int line);
ULONG dab_affected_rows(void);

int dab_begin(const char *file, int line, int *txn);
int dab_commit(const char *file, int line, int *txn);
int dab_rollback(const char *file, int line, int *txn);

#endif

