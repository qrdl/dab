/**************************************************************************
 *
 *  File:       dab_threads.h
 *
 *  Project:    Dab (https://gitlab.com/qrdl/dab)
 *
 *  Descr:      DB-independent data access library
 *
 *  Comments:   Thread-related stuff
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#ifndef DAB_THREADS_H
#define DAB_THREADS_H

#include <pthread.h>

#define READ_LOCK(F,L) do { \
    int err = pthread_rwlock_rdlock(&db_lock); \
    if (err) { \
        LOCAL_LOG('E', F, L, "Error getting read lock: %s", strerror(err)); \
        ret = DAB_MALFUNCTION; \
    } \
} while (0)

#define WRITE_LOCK(F,L) do { \
    int err = pthread_rwlock_wrlock(&db_lock); \
    if (err) { \
        LOCAL_LOG('E', F, L, "Error getting write lock: %s", strerror(err)); \
        ret = DAB_MALFUNCTION; \
    } \
} while (0)

#define UNLOCK(F,L) do { \
    int err = pthread_rwlock_unlock(&db_lock); \
    if (err) { \
        LOCAL_LOG('E', F, L, "Error releasing lock: %s", strerror(err)); \
        ret = DAB_MALFUNCTION; \
    } \
} while (0)

#define TXN_LOCK(F,L) do { \
    int err = pthread_mutex_lock(&txn_mutex); \
    if (err) { \
        LOCAL_LOG('E', file, line, "Error locking mutex: %s", strerror(err)); \
        ret = DAB_MALFUNCTION; \
    } \
} while (0)

#define TXN_UNLOCK(F,L) do { \
    int err = pthread_mutex_unlock(&txn_mutex); \
    if (err) { \
        LOCAL_LOG('E', file, line, "Error unlocking mutex: %s", strerror(err));\
        ret = DAB_MALFUNCTION; \
    } \
} while (0)

#endif

