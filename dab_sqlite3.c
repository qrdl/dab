/**************************************************************************
 *
 *  File:       dab_sqlite.c
 *
 *  Project:    Dab (https://gitlab.com/qrdl/dab)
 *
 *  Descr:      DB-independent data access library
 *
 *  Comments:   SQLite3 access
 *
 **************************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Ilya Caramishev
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#include <sqlite3.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include "dab.h"
/* Requires EEL error handling and logging lib - https://gitlab.com/qrdl/eel */
#include "eel.h"
#ifdef STINGRAY
#include "stingray.h"
#endif

#define STMT(A) ((sqlite3_stmt *)(A))
#define P_STMT(A) ((sqlite3_stmt **)(A))
#define UNUSED(A) (void)(A)

#define DBERR(A) LOCAL_LOG('E', file, line, "%s - %s (%d)", (A), \
        sqlite3_errmsg(db), sqlite3_errcode(db))

static int sql_common(const char *file, int line, sqlite3_stmt **stmt,
        const char *stmt_text, va_list ap);
static int cursor_bind(const char *file, int line, sqlite3_stmt *stmt,
        va_list ap);

static sqlite3 *db = NULL;      // DB handle

#ifndef NOTHREADS
#include "dab_threads.h"
static pthread_mutex_t txn_mutex;
static pthread_rwlock_t db_lock = PTHREAD_RWLOCK_INITIALIZER;
#else
#define READ_LOCK(F,L)
#define WRITE_LOCK(F,L)
#define UNLOCK(F,L)
#define TXN_LOCK(F,L)
#define TXN_UNLOCK(F,L)
#endif

/**************************************************************************
 *
 *  Function:   dab_open
 *
 *  Params:     db_name - database file name
 *              user (not used)
 *              password - not used
 *              flags - see DAB_FLAG_XXX
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL / DAB_MALFUNCTION
 *
 *  Descr:      Connect to db using provided credentials
 *
 *  Note:       SQLite doesn't make use of extra parameters, so there is no
 *              need to proccess them
 *
 **************************************************************************/
int dab_open(const char *file, int line, const char *db_name, ULONG flags,
        const char *connect_string) {
    int ret = DAB_OK;
    UNUSED(connect_string);

    if (!db_name)
        return DAB_INVALID;

    int sqlite_flags = 0;
    if (flags & DAB_FLAG_READONLY)
        sqlite_flags |= SQLITE_OPEN_READONLY;
    else
        sqlite_flags |= SQLITE_OPEN_READWRITE;
    if (flags & DAB_FLAG_CREATE)
        sqlite_flags |= SQLITE_OPEN_CREATE;

    WRITE_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (db) {
        LOCAL_LOG('E', file, line, "DB is already opened");
        RETCLEAN(DAB_INVALID);
    }

    /* create/open ODB */
    ret = sqlite3_open_v2(db_name, &db, sqlite_flags, NULL);
    if (SQLITE_OK != ret) {
        DBERR("Error opening DB");
        if (SQLITE_CANTOPEN == ret)
            RETCLEAN(DAB_INVALID);
        else
            RETCLEAN(DAB_FAIL);
    }
    ret = DAB_OK;

#ifndef NOTHREADS
    pthread_mutexattr_t attr;
    int thread_err;
    thread_err = pthread_mutexattr_init(&attr);
    if (thread_err) {
        LOCAL_LOG('E', file, line, "Mutex attr init failed - %s",
                strerror(thread_err));
        RETCLEAN(DAB_MALFUNCTION);
    }
    thread_err = pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    if (thread_err) {
        LOCAL_LOG('E', file, line, "Mutex attr recursive failed - %s",
                strerror(thread_err));
        RETCLEAN(DAB_MALFUNCTION);
    }
    thread_err = pthread_mutex_init(&txn_mutex, &attr);
    if (thread_err) {
        LOCAL_LOG('E', file, line, "Txn mutex init failed - %s",
                strerror(thread_err));
        RETCLEAN(DAB_MALFUNCTION);
    }
#endif

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_close
 *
 *  Params:     flags - see DAB_FLAG_DEFER
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_MALFUNCTION
 *
 *  Descr:      Close DB connection
 *
 *  Note:       In case of graceful close connection still be active
 *              until all prepared statements are freed, but will not be
 *              reachable for new operations, DB needs to be re-opened
 *              to run new statements.
 *
 **************************************************************************/
int dab_close(const char *file, int line, int flag) {
    int ret = DAB_OK;

    WRITE_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_OK);      // already closed - nothing to do

    if (DAB_FLAG_GRACEFUL & flag) {
        if (SQLITE_OK != sqlite3_close_v2(db)) {
            DBERR("Error closing DB");
            RETCLEAN(DAB_FAIL);
        }
    } else {    // close immediate
        /* rollback active transaction */
        if (!sqlite3_get_autocommit(db)) {   // indicates whether in txn or not
            int tmp = 1;
            LOCAL_LOG('W', file, line, "Active transaction is rolled back "
                    "because of DB close");
            dab_rollback(file, line, &tmp);
        }

        /* destory all prepared statements */
        sqlite3_stmt *stmt;
        for (stmt = sqlite3_next_stmt(db, NULL); stmt;
                stmt = sqlite3_next_stmt(db, NULL)) {
            LOCAL_LOG('W', file, line, "Finalising prepared statement because "
                    "of DB close: %s", sqlite3_sql(stmt));
            sqlite3_finalize(stmt);
        }

        /* actual close */
        if (SQLITE_OK != sqlite3_close(db)) {
            DBERR("Error closing DB");
            RETCLEAN(DAB_FAIL);
        }
    }

    db = NULL;

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_exec
 *
 *  Params:     stmt_text - SQL statement text
 *              ... - pair of params, each pair consists of type ID and
 *                    parameter
 *                    type 0 means "no more params"
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      My own SQLite one-function exec that does everything -
 *              prepares the statement, binds all params, executes the
 *              statement and frees it
 *              Because statement is prepared, there is no need to protect
 *              from SQL injection
 *
 **************************************************************************/
int dab_exec(const char *file, int line, const char *stmt_text, ...) {
    int ret = DAB_OK;
    sqlite3_stmt *stmt = NULL;
    va_list ap;

    if (!stmt_text)
        return DAB_INVALID;

    va_start(ap, stmt_text);

    ret = sql_common(file, line, &stmt, stmt_text, ap);
    if (DAB_OK != ret)
        RETCLEAN(ret);

    if (SQLITE_DONE != sqlite3_step(stmt)) {
        DBERR("Error executing statement");
        RETCLEAN(DAB_FAIL);
    }

cleanup:
    va_end(ap);

    if (stmt)
        sqlite3_finalize(stmt);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_open
 *
 *  Params:     stmt (OUT) - where to store prepared statement
 *              stmt_text - SQL statement text
 *              ... - pair of params, each pair consists of type ID and
 *                    parameter
 *                    type 0 means "no more params"
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      Create and bind the cursor
 *
 **************************************************************************/
int dab_cursor_open(const char *file, int line, void **stmt,
        const char *stmt_text, ...) {
    int ret = DAB_OK;
    va_list ap;

    if (!stmt || !stmt_text)
        return DAB_INVALID;

    va_start(ap, stmt_text);

    ret = sql_common(file, line, P_STMT(stmt), stmt_text, ap);

    va_end(ap);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_prepare
 *
 *  Params:     stmt (OUT) - where to store prepared statement
 *              stmt - SQL statement text
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID
 *
 *  Descr:      Common code for dab_exec() and dab_cursor_open()
 *
 **************************************************************************/
int dab_cursor_prepare(const char *file, int line, void **stmt, const char *stmt_text) {
    int ret = DAB_OK;

    if (!stmt || !stmt_text)
        return DAB_INVALID;

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_INVALID);

    if (SQLITE_OK != sqlite3_prepare_v2(db, stmt_text, -1, P_STMT(stmt), NULL)) {
        DBERR("Error preparing statement");
        RETCLEAN(DAB_FAIL);
    }

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_last_id
 *
 *  Params:     
 *
 *  Return:     rowid of last inserted record / 0 on error
 *
 *  Descr:      Return ID of last inserted record, if it has AUTOINCREMENT
 *              column
 *
 **************************************************************************/
ULONG dab_last_id(const char *file, int line) {
    int ret = DAB_OK;

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return 0;

    if (!db)
        return 0;

    ULONG row = sqlite3_last_insert_rowid(db);
    if (!row)
        DBERR("Nothing was inserted into DB");

    UNLOCK(file, line);

    if (DAB_OK != ret)
        return 0;
    else
        return row;
}


/**************************************************************************
 *
 *  Function:   dab_affected_rows
 *
 *  Params:     
 *
 *  Return:     number of rows
 *
 *  Descr:      Return numer of rows affected by last successful
 *              INSERT / UPDATE / DELETE
 *
 **************************************************************************/
ULONG dab_affected_rows(void) {
	return sqlite3_changes(db);
}


/**************************************************************************
 *
 *  Function:   dab_cursor_bind
 *
 *  Params:     stmt - prepared SQL statement
 *              ... - pair of params, each pair consists of type ID and
 *                    parameter
 *                    type 0 means "no more params"
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      Re-bind previously created cursor
 *
 **************************************************************************/
int dab_cursor_bind(const char *file, int line, void *stmt, ...) {
    va_list ap;
    int ret = DAB_OK;

    if (!stmt)
        return DAB_INVALID;

    va_start(ap, stmt);

    ret = cursor_bind(file, line, STMT(stmt), ap);

    va_end(ap);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_fetch
 *
 *  Params:     stmt - prepared and bound SQL statement
 *              ... - pair of params, each pair consists of type ID and
 *                    parameter
 *                    type 0 means "no more params"
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL / DAB_NO_DATA
 *
 *  Descr:      Fetch a row from cursor
 *
 **************************************************************************/
int dab_cursor_fetch(const char *file, int line, void *stmt, ...) {
    int ret = DAB_OK;
    va_list ap;
    int type, index = 0;

    if (!stmt)
        return DAB_INVALID;

    va_start(ap, stmt);

    ret = sqlite3_step(STMT(stmt));
    if (SQLITE_DONE == ret)
        RETCLEAN(DAB_NO_DATA);
    else if (SQLITE_ROW != ret) {
        DBERR("Error fetching stmt");
        RETCLEAN(DAB_FAIL);
    } else
        ret = DAB_OK;

    for (type = va_arg(ap, int); type; type = va_arg(ap, int)) {
#define TMP(A) A *tmp = va_arg(ap, A *)
        switch (type) {
            case DB_DATATYPE_INT: {
                    TMP(int);
                    *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_UINT: {
                    TMP(unsigned int);
                    *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_SHRT: {
                    TMP(short);
                    *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_USHRT: {
                    TMP(unsigned short);
                    *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_LNG: {
                    TMP(long);
                    if (8 == sizeof(long))
                        *tmp = sqlite3_column_int64(STMT(stmt), index);
                    else
                        *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_ULNG: {
                    TMP(unsigned long);
                    if (8 == sizeof(unsigned long))
                        *tmp = sqlite3_column_int64(STMT(stmt), index);
                    else
                        *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_LLNG: {
                    TMP(long long);
                    if (8 == sizeof(long long))
                        *tmp = sqlite3_column_int64(STMT(stmt), index);
                    else
                        *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_ULLNG: {
                    TMP(unsigned long long);
                    if (8 == sizeof(unsigned long long))
                        *tmp = sqlite3_column_int64(STMT(stmt), index);
                    else
                        *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_CHR: {
                    TMP(char);
                    *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_UCHR: {
                    TMP(unsigned char);
                    *tmp = sqlite3_column_int(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_STR:
            case DB_DATATYPE_USTR: {
                    TMP(char *);
                    const char * zzz = (const char *)sqlite3_column_text(STMT(stmt),
                                index);
                    if (zzz) {
                        *tmp = strdup(zzz);
                    } else {
                        *tmp = NULL;
                    }
                    break;
                }
            case DB_DATATYPE_FLT: {
                    TMP(float);
                    *tmp = sqlite3_column_double(STMT(stmt), index);
                    break;
                }
            case DB_DATATYPE_DBL: {
                    TMP(double);
                    *tmp = sqlite3_column_double(STMT(stmt), index);
                    break;
                }
#ifdef STINGRAY
            case DB_DATATYPE_SR: {
                    sr_string tmp = va_arg(ap, sr_string);
                    size_t size = sqlite3_column_bytes(STMT(stmt), index);
                    sr_ensure_size(tmp, size, size);
                    memcpy(CSTR(tmp), sqlite3_column_blob(STMT(stmt), index), size);
                    SETLEN(tmp, size);
                    break;
                }
#endif
            default:
                DBERR("Invalid column type");
                RETCLEAN(DAB_INVALID);
                break;
        }
        index++;
    }

cleanup:
    va_end(ap);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_reset
 *
 *  Params:     stmt - prepared and bound SQL statement
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      Reset the cursor, but do not destroy it. To be used again,
 *              cursor must be bound first
 *
 **************************************************************************/
int dab_cursor_reset(const char *file, int line, void *stmt) {
    if (!stmt)
        return DAB_INVALID;

    if (SQLITE_OK != sqlite3_reset(STMT(stmt)) ||
            SQLITE_OK != sqlite3_clear_bindings(STMT(stmt))) {
        DBERR("Error resetting stmt");
        return DAB_FAIL;
    }

    return DAB_OK;
}


/**************************************************************************
 *
 *  Function:   dab_cursor_free
 *
 *  Params:     stmt - prepared SQL statement
 *
 *  Return:     DAB_OK / DAB_INVALID / DAB_FAIL
 *
 *  Descr:      Destory the cursor
 *
 **************************************************************************/
int dab_cursor_free(const char *file, int line, void *stmt) {
    if (!stmt)
        return DAB_INVALID;

    if (SQLITE_OK != sqlite3_finalize(STMT(stmt))) {
        DBERR("Error freeing stmt");
        return DAB_FAIL;
    }

    return DAB_OK;
}


/**************************************************************************
 *
 *  Function:   sql_common
 *
 *  Params:     stmt (OUT) - where to store prepared statement
 *              stmt - SQL statement text
 *              ap - va_list of params
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID / DAB_MALFUNCTION
 *
 *  Descr:      Common code for dab_exec() and dab_cursor_open()
 *
 **************************************************************************/
int sql_common(const char *file, int line, sqlite3_stmt **stmt,
        const char *stmt_text, va_list ap) {
    int ret = DAB_OK;

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_INVALID);

    if (SQLITE_OK != sqlite3_prepare_v2(db, stmt_text, -1, stmt, NULL)) {
        DBERR("Error preparing statement");
        RETCLEAN(DAB_FAIL);
    }

    ret = cursor_bind(file, line, *stmt, ap);

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   cursor_bind
 *
 *  Params:     stmt - prepared SQL statement
 *              ap - va_list of params
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID
 *
 *  Descr:      Common code for dab_cursor_bind() and sql_common()
 *
 **************************************************************************/
int cursor_bind(const char *file, int line, sqlite3_stmt *stmt, va_list ap) {
    int type, index = 0;
    int ret;

    for (type = va_arg(ap, int); type; type = va_arg(ap, int)) {
        index++;
        switch (type) {
            case DB_DATATYPE_INT:
            case DB_DATATYPE_SHRT:
            case DB_DATATYPE_CHR:
                ret = sqlite3_bind_int(stmt, index, va_arg(ap, int));
                break;
            case DB_DATATYPE_UINT:
            case DB_DATATYPE_USHRT:
            case DB_DATATYPE_UCHR:
                ret = sqlite3_bind_int(stmt, index, va_arg(ap, unsigned int));
                break;
            case DB_DATATYPE_LNG:
                if (8 == sizeof(long))
                    ret = sqlite3_bind_int64(stmt, index, va_arg(ap, long));
                else
                    ret = sqlite3_bind_int(stmt, index, va_arg(ap, long));
                break;
            case DB_DATATYPE_ULNG:
                if (8 == sizeof(unsigned long))
                    ret = sqlite3_bind_int64(stmt, index,
                            va_arg(ap, unsigned long));
                else
                    ret = sqlite3_bind_int(stmt, index,
                            va_arg(ap, unsigned long));
                break;
            case DB_DATATYPE_LLNG:
                if (8 == sizeof(long long))
                    ret = sqlite3_bind_int64(stmt, index,
                            va_arg(ap, long long));
                else
                    ret = sqlite3_bind_int(stmt, index, va_arg(ap, long));
                break;
            case DB_DATATYPE_ULLNG:
                if (8 == sizeof(unsigned long long))
                    ret = sqlite3_bind_int64(stmt, index,
                            va_arg(ap, unsigned long long));
                else
                    ret = sqlite3_bind_int(stmt, index,
                            va_arg(ap, unsigned long long));
                break;
            case DB_DATATYPE_STR:
            case DB_DATATYPE_USTR:
                ret = sqlite3_bind_text(stmt, index, va_arg(ap, char*), -1,
                        NULL);
                break;
            case DB_DATATYPE_FLT:
            case DB_DATATYPE_DBL:
                ret = sqlite3_bind_double(stmt, index, va_arg(ap, double));
                break;
#ifdef STINGRAY
            case DB_DATATYPE_SR: {
                sr_string tmp = va_arg(ap, sr_string);
                ret = sqlite3_bind_text(stmt, index, CSTR(tmp), STRLEN(tmp),
                        NULL);
                break;
            }
#endif
            default:
                LOCAL_LOG('E', file, line, "Invalid column type for param %d", index);
                return DAB_INVALID;
                break;
        }
        if (SQLITE_OK != ret) {
            DBERR("Error binding param");
            return DAB_FAIL;
        }
    }

    /* check if number of '?' placeholders actually match the number of
     * params */
    if (sqlite3_bind_parameter_count(stmt) != index) {
        LOCAL_LOG('E', file, line, "Number of params doesn't match number of "
                "placeholders");
        return DAB_INVALID;
    }

    return DAB_OK;
}


/**************************************************************************
 *
 *  Function:   dab_begin
 *
 *  Params:     txn - pointer to local transaction indicator
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID / DAB_MALFUNCTION
 *
 *  Descr:      Begin local transaction
 *
 **************************************************************************/
int dab_begin(const char *file, int line, int *txn) {
    int ret = DAB_OK;

    if (*txn) {
        LOCAL_LOG('E', file, line, "Local transaction is already in progress");
        return DAB_INVALID;
    }

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;
    
    if (!db)
        RETCLEAN(DAB_INVALID);

    if (sqlite3_get_autocommit(db)) {   // indicates whether in txn or not
        TXN_LOCK(file, line);
        if (DAB_OK != ret)
            RETCLEAN(ret);
        if (SQLITE_OK != sqlite3_exec(db, "BEGIN TRANSACTION", NULL, 0, NULL)) {
            DBERR("Cannot start transaction");
            TXN_UNLOCK(file, line);
            if (DAB_OK != ret)
                RETCLEAN(ret);
            else
                RETCLEAN(DAB_FAIL);
        }
        *txn = 1;   // in local transaction
    }

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_commit
 *
 *  Params:     txn - pointer to local transaction indicator
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID / DAB_MALFUNCTION
 *
 *  Descr:      Commit local transaction
 *
 **************************************************************************/
int dab_commit(const char *file, int line, int *txn) {
    int ret = DAB_OK;

    if (!*txn)
        return DAB_OK;  // not in transaction

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_INVALID);

    if (SQLITE_OK != sqlite3_exec(db, "COMMIT", NULL, 0, NULL)) {
        DBERR("Cannot commit transaction");
        RETCLEAN(DAB_FAIL);
    }
    *txn = 0;

    TXN_UNLOCK(file, line);
    if (DAB_OK != ret)
        RETCLEAN(DAB_MALFUNCTION);

cleanup:
    UNLOCK(file, line);

    return ret;
}


/**************************************************************************
 *
 *  Function:   dab_rollback
 *
 *  Params:     txn - pointer to local transaction indicator
 *
 *  Return:     DAB_OK / DAB_FAIL / DAB_INVALID / DAB_MALFUNCTION
 *
 *  Descr:      Rollback local transaction
 *
 **************************************************************************/
int dab_rollback(const char *file, int line, int *txn) {
    int ret = DAB_OK;

    if (!*txn)
        return DAB_OK;  // not in transaction

    READ_LOCK(file, line);
    if (DAB_OK != ret)
        return ret;

    if (!db)
        RETCLEAN(DAB_INVALID);

    if (SQLITE_OK != sqlite3_exec(db, "ROLLBACK", NULL, 0, NULL)) {
        DBERR("Cannot rollback transaction");
        RETCLEAN(DAB_FAIL);
    }
    *txn = 0;

    TXN_UNLOCK(file, line);
    if (DAB_OK != ret)
        RETCLEAN(DAB_MALFUNCTION);

cleanup:
    UNLOCK(file, line);

    return ret;
}

