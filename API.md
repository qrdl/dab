## Header
Include [dab.h](dab.h) into your source, and link with required provider

## Return codes
All Dab macros return one of following codes:
- `DAB_OK` - operation successful
- `DAB_FAIL` - database error, error logged using Eel
- `DAB_INVALID` - invalid parameter or set of parameters was provided by
consumer code, or number of passed host variables do not correspond with number
of placeholders/columns, or some other error that most probably caused by
misconfiguration. Error may be logged, if problem is not obvious
- `DAB_NO_DATA` - no data found, cursor has come to the end of result set
- `DAB_MALFUNCTION` - system malfunction (such as mutex lock/unlock failure),
most likely application cannot continue

## Database manipulation
#### Opening database
`DAB_OPEN(db_name, flags, connect_string);`  
`connect_string` differs for different providers, and not all flags are
supported for all providers, see [DBMSes.md](DBMSes.md) for details.

Flags - one or more ORed contants:
- `DAB_FLAG_READONLY` (if not specified, DB is opened in read/write mode)
- `DAB_FLAG_CREATE` (database will be created, if it does not exist)
Not all flags are supported by all DB providers

#### Closing database
`DAB_CLOSE(flags)`  
The only flag supported is `DAB_FLAG_GRACEFUL` - that will close database
gracefully, all prepared and non-released statements still be accessible.

## One-step SQL execution (non-query)
`DAB_EXEC(SQL_statement [, var1 [, var2 [...]]])`  
Statement will be executed with all provided variables bound. Number of
variables must match the number of placeholders in the statement

## Cursors / prepared statements
Cursors primarily is used for queries, and are the only way to do a SELECT.
However, it is possible to prepare non-SELECT statement (UPDATE or DELETE, for
example), and just execute again and again by resetting/rebinding/fetching the
cursor.
#### Creating the cursor
`DAB_CURSOR_OPEN(addr_of_handle, SQL_statement [, var1 [, var2 [...]]])`  
Creates the cursor and binds all provided variables to the statement, sets the
handle. Number of variables must match the number of placeholders in the
statement.

#### Fetching the data from cursor / executing the prepared statement
`DAB_CURSOR_FETCH(handle [, addr_of_var1 [, addr_of_var2 [...]]])`  
Fetch the cursor/execute the prepared statement, store returned data into
variables provided. For string parameters memory will be allocated, and it
must be free'd by consumer code

#### Resetting the cursor / prepared statement
`DAB_CURSOR_RESET(handle)`  
Resets and unbinds open cursor / prepared statement. After this operation
cursor cannot be fetched before it was rebound

#### Rebinding the cursor
`DAB_CURSOR_BIND(handle, [, var1 [, var2 [...]]])`  
Binds all provided variables to the statement. Number of variables must match
the number of placeholders in the statement, previously used for cursor creation

#### Freeing the cursor
`DAB_CURSOR_FREE(handle)`  

## Transactions
Dab assumes that transaction is always finished (commited or rolled back) within
the same function where it was started, therefore Dab uses indicator variable
to mark whether transaction was started within the function or not. It means
it is safe to do a BEGIN in called function when transaction is already in
progress within callee.
Example:
```c
void foo(void) {
    DAB_TXN;    // init local indicator
    if (DAB_OK != DAB_BEGIN)  // txn started
        ...
    bar();  ------------->  void bar(void) {
                                DAB_TXN;    // init local indicator
                                DAB_BEGIN;  // txn in progress - does nothing
                                ...
                                DAB_COMMIT; // no local txn - does nothing
            <-------------  return; }
    if (DAB_OK != DAB_COMMIT)   // try to commit
        DAB_ROLLBACK();         // commit failed - rollback
    ...
```
But if function `bar()` is called while transaction is not running, it will
initiate its own transaction, and COMMIT/ROLLBACK would work.

As you see from the example, there are 4 macros needed for transactions:
`DAB_TXN` - initialise local transaction indicator
`DAB_BEGIN` - start transaction, if no transaction is running, return status
`DAB_COMMIT` - commit local transaction, if it was started, return status
`DAB_ROLLBACK` - rolls back local transaction, if it was started, return status

## Miscellaneous
#### Value of autoincremented field from last INSERT
`DAB_LAST_ID`  
Returns `uint64_t` with value, or 0 on error

## Threads / concurrency
Dab is thread-safe, running several SQL statements in parallel is ok (although
it still can produce some race conditions on DBMS level). In case of single-
threaded application (or on platform that does not support POSIX threads)
thread support can be switched off by defining `NOTHREADS` macro.

#### Single database
Database handle is a global pointer, and it is protected by read-write lock, so
opening/closing database while doing queries in other threads will no crash.
Dab can work with one database at a time. However, after database is closed,
another one can be connected. If first database is closed gracefully (see
Memory management section) and there are some prepared statements left, they
can be used even if another database was opened.

#### Concurrent transactions
By defauls Dab serialises all transactions with the help of a mutex (it can
be switched off by defining `NOTHREADS`). Be aware that it may result in
deadlocks, so application programmer must be careful with lock strategy.
If multi-threaded application is using transactions, it is highly recommended
to wrap ALL data changes into transactions, otherwise it may result in very
hard-to-track-down bugs, like some UPDATE didn't produce an error, but data
wasn't changed because some transaction in other thread was rolled back in
the same time.

## Memory management, leaks and crashes
The only resourse that Dab allocates and doesn't free itself are cursor
(prepared statements) - application code is responsible to call
`DAB_CURSOR_FREE()` to release memory, allocated for prepared statement.
If database if closed gracefully (with flag `DAB_FLAG_GRACEFUL` set), prepared
statements are not released and still can be used, however no new queries can
be made, and application code still has to release these statements to avoid
resource leaks.
Closing database ungracefully (default) will release all prepared statements,
and any attempt to use previously-prepared statement will cause undefined
behaviour.

