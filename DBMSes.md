## Features partially supported
Not all Dab features are supported by all DBMSes, although it may be caused
just by author's lack of knowledge.
The table below shows these descrepancies.

| Feature                                                   | sqlite3 | mysql |
|:----------------------------------------------------------|:-------:|:-----:|
| Database creation, if does not exist (`DAB_FLAG_CREATE`)  | Yes     | No    |
| Openning database in read-only mode (`DAB_FLAG_READONLY`) | Yes     | No    |
| Graceful close (`DAB_FLAG_GRACEFUL`)                      | Yes     | No    |

## Connect string
Different DBMSes use different parameters for connecting to DB, thus there are.
different paramaters in connect strings.

#### sqlite3
sqlite3 doesn't require any parameters in connect string, as it is embedded
database

#### MySQL/MariaDB
There are number of parameters user can set for MySQL/MariaDB DBMS:
- `host` - host name or IP address, if omitted, `localhost` is assumed
- `user` - user name, if omitted, current user is assumed
- `password` - user password, if omitted, blank password is assumed
- `port` - TCP port, if omitted, UNIX socket commection is assumed
- `socket` - UNIX socket, if omitted, TCP connection is assumed
Either `port` or `socket` must be specified. Failure to specify both will
produce an error.

