# Dab
Dab is simple cross-DBMS SQL data access library, it provides single C API
that allows to use any supported database

## Features

#### API features
- Common API for different DBMSes allows to switch DBMS easily (although SQL
syntax may vary)
- Dab uses C11 Generics, it allows to greatly simplify application code
- all host variables get bound to statements, not inserted inside SQL statement
strings, eliminating the risk of SQL injections
- thread-safe (see more in Threads section)
- supports single-call EXEC function for non-SELECT statements
- uses prepared statements as data cursor, thus statements may re-bound and
re-used, to save some CPU cycles
- supports DB transactions
- all errors are logged using Eel library, and error location indicated as line
within application code where Dab macro was called from
- easy to use, in comparison with DBMS APIs

## Compatibility

#### DBMSes
- SQLite 3 (provider name sqlite3)
- MySQL / MariaDB (provider name mysql)

Please note that not all DBMSes support all Dab features - see feature table in
[DBMSes.md](DBMSes.md) for details

#### Compilers
Dab is written in standard C11 with some GCC extentions, also supported by
Clang, therefore can be compiled with either compiler.

#### Platforms
Besides C11 standard library, if `NOTHREADS` macro is undefined, POSIX threads
API is used, therefore Dab can be used on any POSIX-compliant platfrom.

## Dependencies
Naturally, Dab links with DBMS library of chosen provider.
If `NOTHREADS` was not defined, `pthreads` should be linked to binary as well.
NB! If Dab is compiled with `NOTHREADS`, you MUST define `NOTHREADS` before
including Dab's header!

Dab uses sister library Eel for logging and error handling, it can be obtained
at https://gitlab.com/qrdl/eel.

Optionally Dab uses sister library Stingray for managed strings, if compiled
with STINGRAY defined. It allows to use managed strings as SQL host variables,
and can be obtained at https://gitlab.com/qrdl/stingray.

## Compiling
To compile all providers and test harnesses, run `make`.
To compile just single provider, type `make <db>`, where `<db>` - provider name.
To link an application with provider, add provider's object file to your linking
command, and do not forget to link with database library. For example, if you
linked your application like
`gcc -o app file1.o file2.o`,
you would need to change it to
`gcc -o app file1.o file2.o dab/dab_sqlite3.o -lsqlite3`
in case of SQLite 3 database provider
See [make](Makefile) for recommended compilation and linking commands.

#### Stringray support
To compile Dab with Stingray support, pass `EXTRA` parameter to `make`, like
`make EXTRA="-DSTINGRAY -I../stingray ../stingray/stingray.o"`, where
`-DSTINGRAY` - macro to switch on Stingray support, `-I../stingray` - path to
Stingray include file and `../stingray/stingray.o` - Stingray object file to
link.

NB!  Make sure you include Stringray's header before Dab's one in you core in
order to use Stingray managed strings as host variables in Dab.

## Use
In order to use Dab you need to include `dab.h` into your source file. Do not use
Dab functions directly, use `DAB_XXX` macros to have correct error logging.
For details see [API description](API.md).
Example can be found in [test](test.c)

## Limitations
- Only one Dab provider can be linked, so application cannot access two DBMSes
- only one database can be connected at a time
- Dab uses C11's Generics, therefore it cannot be used in projects that require
following pre-C11 standards (like C99).

## License
This project is licensed under the MIT License - see the [license](LICENSE) for
details

## Author(s)
- Ilya Caramishev - https://gitlab.com/qrdl
