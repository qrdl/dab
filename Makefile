##########################################################################
#
#  File:       Makefile
#
#  Project:    Dab (https://gitlab.com/qrdl/dab)
#
#  Descr:      Database access
#
#  Comments:   Makefile for Dab
#
##########################################################################
#
#  The MIT License (MIT)
#
#  Copyright (c) 2017 Ilya Caramishev
#
#  Permission is hereby granted, free of charge, to any person obtaining a
#  copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,
#  and/or sell copies of the Software, and to permit persons to whom the
#  Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
#  DEALINGS IN THE SOFTWARE.
#
##########################################################################/
CFLAGS := -Wall -Wextra -g3 -I../eel $(EXTRA)
LDLIBS := -lsqlite3
ifndef CC
    CC := gcc
endif

.PHONY : all clean sqlite3 mysql

all: sqlite3 mysql

test.o: test.c dab.h Makefile

dab.h: dab_internal.h

######## SQLite
sqlite3: dab_sqlite3.o test_sqlite3

dab_sqlite3.o: dab_sqlite3.c dab.h dab_internal.h dab_threads.h Makefile
	$(CC) $(CFLAGS) -c -o $@ dab_sqlite3.c

test_sqlite3: test.c dab_sqlite3.o
	$(CC) $(CFLAGS) -DSQLITE -o $@ -pthread $^ $(LDLIBS)

####### MySQL / MariaDB

mysql: dab_mysql.o test_mysql

dab_mysql.o: dab_mysql.c dab.h dab_internal.h dab_threads.h Makefile
	$(CC) $(CFLAGS) $(shell mysql_config --cflags) -c -o $@ dab_mysql.c

test_mysql: test.c dab_mysql.o
	$(CC) $(CFLAGS) -DMYSQL -o $@ $(shell mysql_config --libs) $^

clean:
	rm -f test_* dab_*.o

dab.h: dab_internal.h
